#encoding: utf-8
require 'rubygems'
require 'bundler/setup'
Bundler.require
require_relative 'settings'
require_relative 'tools'

class RestartServer
  PATH = File.dirname(Pathname.new(__FILE__).realpath.to_path)

  def self.run
    process_file = File.join(File.dirname(PATH), "log", "process.lock")

    # 1. 随机等待0～60分钟
    sleep rand(1..3600)

    # 2. 删除进程锁文件
    Tools.delete_process_file(process_file)

    # 3. 重启服务器
    `sudo shutdown -r now`
  end

end