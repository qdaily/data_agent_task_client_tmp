#encoding: utf-8
require 'rubygems'
require 'bundler/setup'
Bundler.require
require_relative 'settings'
require_relative 'tools'
require_relative 'click'
require_relative 'impression'

class PptpTask
  @@logger = Logger.new(File.expand_path('../../log/pptp_task.log', __FILE__), 3, 10485760)
  PATH = File.dirname(Pathname.new(__FILE__).realpath.to_path)
  VERSION = "2.0.1"
  IMG_PATH = "/home/data_agent/Desktop/"

  def self.run
    count = 2
    pptp_task = PptpTask.new
    server_title = Socket.gethostname
    token = Digest::MD5::hexdigest("data_agent" + Time.now.strftime("%Y-%m-%d-%H"))
    # process_file = File.join(File.dirname(PATH), "log", "process_pptp.lock")

    # 1. 检测系统性能（ok）
    @@logger.warn "开始检测系统负载"
    return @@logger.warn "系统负载已满".red if Tools.check_performance
    @@logger.warn "系统负载检测通过".green

    # 2. 检测唯一进程（ok）
    @@logger.warn "开始检查是否是唯一进程"
    process_count = `ps aux|grep run_pptp_task|grep ruby |wc -l`
    @@logger.warn "当前进程数：#{process_count}"
    return @@logger.warn "不是唯一进程".red if process_count.to_i > 2
    @@logger.warn "唯一进程检测通过".green

    count.times do |i|
      begin
        @@logger.warn "开始第#{i+1}次任务"

        # 3. 关闭变换IP（ok）
        @@logger.warn "开始关闭pptp"
        `sudo poff myvpn`
        sleep 2
        @@logger.warn "关闭pptp成功".green

        # 4. 获取账户（ok）
        @@logger.warn "开始获取账户"
        get_account_result = pptp_task.get_account(server_title, token, VERSION)
        interval_time = get_account_result["interval_time"].to_i
        next @@logger.warn "获取账户失败".red if get_account_result["state"] == false
        @@logger.warn "获取账户成功".green

        # 5. 启动pptp
        @@logger.warn "开始启动pptp"
        begin_time = Time.now.to_i
        change_ip_result = pptp_task.change_ip(get_account_result["account"])
        if change_ip_result == false
          @@logger.warn "启动pptp失败".red
          task_time = Time.now.to_i - begin_time
          sleep (interval_time - task_time) if task_time < interval_time
          next
        end
        @@logger.warn "启动pptp成功".green

        # 6. 检测ip（ok）
        @@logger.warn "开始检测ip"
        check_ip_result = Tools.check_ip(@@logger, server_title, token)
        if check_ip_result == false
          @@logger.warn "检测ip失败".red
          `sudo poff myvpn`
          task_time = Time.now.to_i - begin_time
          sleep (interval_time - task_time) if task_time < interval_time
          next
        end
        @@logger.warn "检测ip成功".green

        Tools.check_performance_and_wait
        # 7. 检测网速（ok）
        @@logger.warn "开始检测网速"
        check_network_result = Tools.check_network(@@logger)
        if check_network_result == false
          @@logger.warn "检测网速失败".red
          `sudo poff myvpn`
          task_time = Time.now.to_i - begin_time
          sleep (interval_time - task_time) if task_time < interval_time
          next
        end
        @@logger.warn "检测网速成功".green

        # 8. 获取任务内容（ok）
        @@logger.warn "开始获取任务内容"
        task = pptp_task.get_task

        # 9. 关闭chrome进程（ok）
        @@logger.warn "开始关闭chrome进程"
        `killall chrome`
        @@logger.warn "关闭chrome进程成功".green

        Tools.check_performance_and_wait

        # 10. 下载图片
        pptp_task.download_pic

        # 11. 执行任务（ok）
        device_id_status = true
        if task[:task_type] == "click"#点击任务
          @@logger.warn "开始执行点击任务"
          Click.click(task, @@logger)
          @@logger.warn "执行点击任务成功".green
        elsif task[:task_type] == "impression"#曝光任务
          @@logger.warn "开始执行曝光任务"
          Impression.impression(task, @@logger)
          @@logger.warn "执行曝光任务成功".green
        elsif task[:task_type] == "click_with_device_id"#点击任务（带设备号）
          @@logger.warn "开始执行点击任务（带设备号）"
          device_id_status = Click.click(task, @@logger, true)
          @@logger.warn "执行点击任务（带设备号）#{device_id_status ? '成功' : '失败'}".green
        elsif task[:task_type] == "impression_with_device_id"#曝光任务（带设备号）
          @@logger.warn "开始执行曝光任务（带设备号）"
          device_id_status = Impression.impression(task, @@logger, true)
          @@logger.warn "执行曝光任务（带设备号）#{device_id_status ? '成功' : '失败'}".green
        else
          next
        end
        next unless device_id_status

        # 12. 上传任务结果
        @@logger.warn "开始上传任务结果"
        statistic_url = File.join(Settings.defaults.data_agent, "/api/visits/statistic_visit?task_id=#{task[:id]}&account_id=#{get_account_result["account"]["account_id"]}&token=#{token}")
        open(statistic_url, "User-Agent" => task[:useragent][:content])
        @@logger.warn "上传任务结果成功".green

        # # 13. 重置进程锁文件
        # @@logger.warn "开始重置进程锁文件"
        # Tools.set_count(process_file)
        # @@logger.warn "重置进程锁文件成功"

        task_time = Time.now.to_i - begin_time
        sleep (interval_time - task_time) if task_time < interval_time

        @@logger.warn "第#{i+1}次任务完成".green
      rescue => e
        `killall chrome`
        @@logger.warn "第#{i+1}次任务失败：#{e}".red
      end
    end

    # 13. 关闭变换IP
    @@logger.warn "开始关闭pptp"
    `sudo poff myvpn`
    sleep 2
    @@logger.warn "关闭pptp成功".green

    # 14. 删除进程锁文件
    # @@logger.warn "开始删除进程锁文件"
    # Tools.delete_process_file(process_file)
    # @@logger.warn "删除进程锁文件成功".green
  end

  def get_account(server_title, token, version)#ok
    result = {"state" => false}
    begin
      Timeout.timeout(10, Errno::ETIMEDOUT) do
        url = File.join(Settings.defaults.data_agent, "/api/pptp_accounts/show_by_task_client_title?task_client_title=#{server_title}&token=#{token}&task_client_version=#{version}")
        result = JSON.parse(open(url).read)
      end
    rescue => e
      @@logger.warn "获取账户失败：#{e}".red
    end
    return result
  end

  def get_task#ok
    i = rand(1..147)
    ua_str = `sed -n '#{i}p' #{PATH + '/ua.txt'}`.gsub(/\n/, '')
    ua = ua_str.split("&&&&")
    result = {
      id: 58,
      title: "Rayban眼镜（带头像）",
      task_type: "click",
      browser_type: "app",
      app_type: "all",
      source_url: "",
      window_name: "自由同型",
      click_url: "http://dataad.somecoding.com/tasks/click/09ae8ed5-8a90-48d0-8b71-6f96bb5111ab",
      async_click_url: "",
      residence_time: 20,
      impression_url: "",
      impression_rate: 3,
      useragent: {
        content: ua.first,
        browser_width: ua[1],
        browser_height: ua.last
      },
      device: {
        device_id: "",
        useragent: "",
        app_type: ""
      },
      operations: [
        {
          operation_type: "mouse_click",
          content: "207&&646&&5&&5",
          residence_time: 5,
          probability: 100
        },
        {
          operation_type: "mouse_click",
          content: "235&&318&&0&&0",
          residence_time: 5,
          probability: 100
        },
        {
          operation_type: "mouse_click",
          content: "230&&493&&0&&0",
          residence_time: 5,
          probability: 100
        },
        {
          operation_type: "mouse_click",
          content: "318&&490&&0&&0",
          residence_time: 5,
          probability: 100
        },
        {
          operation_type: "mouse_click",
          content: "211&&319&&0&&0",
          residence_time: 5,
          probability: 100
        },
        {
          operation_type: "mouse_click",
          content: "203&&684&&5&&5",
          residence_time: 10,
          probability: 100
        }
      ]
    }
    return result
  end

  def change_ip(account)#ok
    result = false
    begin
      Timeout.timeout(30, Errno::ETIMEDOUT) do
        #修改.sh
        `sudo poff myvpn`
        command = "source /home/change_ip.sh #{account["url"]} #{account["username"]} #{account["password"]}"
        system("bash -c #{command.inspect}")
        sleep 2
        #开启pptp
        `sudo pon myvpn`
        20.times do |i|
          if `route -n`.include?("ppp0")
            result = true
            break
          end
          sleep 1
        end
        raise "pptp start failed" unless result
      end
    rescue => e
      @@logger.warn "启动pptp失败：#{e}".red
    end
    `sudo poff myvpn` if result == false
    return result
  end

   def download_pic
    # 先将目录里的已存在的图片删除
    @@logger.warn "下载头像图片前删除目录中所有图片"
    Dir.foreach(IMG_PATH) do |f|
      next unless f =~ /jpg|png|jpeg|gif/i
      File.delete(IMG_PATH + '/' + f)
    end

    # 获取图片链接
    @@logger.warn "开始下载头像图片"
    data = JSON.parse open(File.join(Settings.defaults.data_agent, "/api/tasks/show_pic")).read
    img_url = data.dig("url")
    
    # 下载图片
    file_name = img_url.split("/").last
    file = File.new(IMG_PATH + file_name, 'w+')
    file.binmode
    data = open(File.join(Settings.defaults.data_agent, "system", file_name)).read
    file << data
    file.flush
    file.close
  end

end
