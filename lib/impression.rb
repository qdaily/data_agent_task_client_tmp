#encoding: utf-8
require_relative 'tools'

class Impression
  PATH = File.dirname(Pathname.new(__FILE__).realpath.to_path)
  ALL_RESIDENCE_TIME = 60

  def self.impression(task, logger, with_device_id=false, proxy_ip=nil, proxy_username=nil, proxy_password=nil)
    #基本参数
    source_url = task[:source_url].split("&&").shuffle[0]
    impression_url = task[:impression_url]
    impression_rate = task[:impression_rate].to_i + [-2,-1,-1,0,0,0,0,1,1,2].sample
    user_agent = task[:useragent][:content]
    if with_device_id
      if task[:device][:device_id].to_s.length < 2 or !%w(iOS android).include?(task[:device][:app_type])
        logger.warn "设备号不正确".red
        return false
      end
      impression_url = impression_url.gsub(task[:device][:app_type] == "iOS" ? "__IDFA__" : "__IMEI__", task[:device][:device_id])
      user_agent = task[:device][:useragent]
    end

    #open设置
    open_options = {"User-Agent" => user_agent}
    open_options = open_options.merge("Referer" => source_url) if (source_url and !source_url.empty?)
    open_options = open_options.merge(:proxy_http_basic_authentication => [URI.parse("http://#{proxy_ip}:9928"), proxy_username, proxy_password]) if proxy_ip

    begin
      Timeout.timeout(ALL_RESIDENCE_TIME, Errno::ETIMEDOUT) do
        for m in 1..impression_rate
          open(impression_url, open_options)
          sleep rand(4..6)
        end
      end
    rescue => e
      logger.warn "执行曝光任务失败：#{e}".red
    end
    return true # device_id is correct
  end
end

