require_relative File.expand_path("../../pptp_task", __FILE__)
require_relative File.expand_path("../../vps_task", __FILE__)
require_relative File.expand_path("../../ping_pptp_task", __FILE__)
ENV['DISPLAY'] = ":1"

desc "run data agent pptp task process"
task :run_pptp_task do
  PptpTask.run
end

desc "run data agent vps task process"
task :run_vps_task do
  VpsTask.run
end

desc "run data agent ping_pptp task process"
task :run_ping_pptp_task do
  PingPptpTask.run
end
