#encoding: utf-8
require_relative 'tools'
require_relative 'settings'

class Click
  PATH = File.dirname(Pathname.new(__FILE__).realpath.to_path)
  ALL_RESIDENCE_TIME = 420
  IMG_PATH = "/home/data_agent/Desktop/"

  def self.click(task, logger, with_device_id=false, proxy_ip=nil, proxy_username=nil, proxy_password=nil)
    #基本参数
    source_url = task[:source_url].split("&&").shuffle[0]
    click_url = task[:click_url]
    async_click_url = task[:async_click_url]
    residence_time = task[:residence_time].to_i * (1+rand(-0.2..0.2))
    impression_url = task[:impression_url]
    user_agent = task[:useragent][:content]
    if with_device_id
      if task[:device][:device_id].to_s.length < 2 or !%w(iOS android).include?(task[:device][:app_type])
        logger.warn "设备号不正确".red
        return false
      end
      click_url = click_url.gsub(task[:device][:app_type] == "iOS" ? "__IDFA__" : "__IMEI__", task[:device][:device_id])
      async_click_url = async_click_url.gsub(task[:device][:app_type] == "iOS" ? "__IDFA__" : "__IMEI__", task[:device][:device_id])
      impression_url = impression_url.gsub(task[:device][:app_type] == "iOS" ? "__IDFA__" : "__IMEI__", task[:device][:device_id])
      user_agent = task[:device][:useragent]
    end

    #open设置
    open_options = {"User-Agent" => user_agent}
    open_options = open_options.merge("Referer" => source_url) if (source_url and !source_url.empty?)
    open_options = open_options.merge(:proxy_http_basic_authentication => [URI.parse("http://#{proxy_ip}:9928"), proxy_username, proxy_password]) if proxy_ip

    #browser设置
    options = Selenium::WebDriver::Chrome::Options.new
    options.add_emulation(user_agent: user_agent)
    options.add_argument("disk-cache-dir=#{File.join(File.dirname(PATH),'cache')}") if rand(10) > 3
    if proxy_ip
      options.add_argument("--proxy-server=#{proxy_ip}:9928")
      options.add_argument("homepage=#{Settings.defaults.data_agent}homes")
    end
    if task[:browser_type] == "app" or task[:browser_type] == "mobile"
      options.add_emulation(device_metrics: {width: 375, height: 667, pixelRatio: 2, touch: true})
    end
    driver = Selenium::WebDriver.for(:chrome, options: options)
    browser = Watir::Browser.new driver
    if task[:browser_type] == "app" or task[:browser_type] == "mobile"
      browser.window.resize_to(750, 1334)
    elsif task[:browser_type] == "pc"
      browser.window.resize_to(1900, 950)
    end

    begin
      Timeout.timeout(ALL_RESIDENCE_TIME, Errno::ETIMEDOUT) do
        Tools.goto_with_auth(proxy_username, proxy_password) if proxy_ip
        impression_and_click(open_options, browser, click_url, async_click_url, impression_url, residence_time, logger)

        #后续操作
        logger.warn "后续操作"
        window_id = `xdotool search #{task[:window_name]}`.split("\n")[0]
        task[:operations].each do |operation|
          # break if rand(100) > operation[:probability]
          if operation[:operation_type] == "js"
            Tools.browser_js(browser, operation[:content])
          elsif operation[:operation_type] == "mouse_click"
            arg = operation[:content].split("&&")
            Tools.mouse_click(window_id, arg[0].to_i, arg[1].to_i, arg[2].to_i, arg[3].to_i)
          elsif operation[:operation_type] == "mouse_move"
            arg = operation[:content].split("&&")
            Tools.mouse_move(window_id, arg[0].to_i, arg[1].to_i, arg[2].to_i, arg[3].to_i)
          elsif operation[:operation_type] == "keyboard_down"
            arg = operation[:content].to_i
            Tools.keyboard_down(arg)
          elsif operation[:operation_type] == "mouse_move_without_sleep"
            arg = operation[:content].split("&&")
            Tools.mouse_move_without_sleep(window_id, arg[0].to_i, arg[1].to_i, arg[2].to_i, arg[3].to_i)
          end
          operation_residence_time = operation[:residence_time].to_i * (1+rand(-0.5..0.5))
          sleep operation_residence_time
        end

        # #重复执行
        # logger.warn "重复执行"
        # impression_and_click(open_options, browser, click_url, async_click_url, impression_url, residence_time, logger) if rand(100) > 87
      
        # 点击上传图片
        page_window_id = `xdotool search 自由同型`.split("\n").first.to_i
        `xdotool mousemove --window #{page_window_id} 235 240 mousedown 1 mouseup 1`
        sleep 3
        `xdotool mousemove --window #{page_window_id} 135 268 mousedown 1 mouseup 1`
        sleep 3
        `xdotool mousemove --window #{page_window_id} 235 240 mousedown 1 mouseup 1 key 36`
      
        sleep 10
        # 就是这张
        Tools.mouse_click(page_window_id, 276, 669, 0, 0)
        sleep 4

        # 墨镜
        Tools.mouse_click(page_window_id, [95, 287].shuffle.first, 630, 0, 0)
        sleep 4

        # 型装完成
        Tools.mouse_click(page_window_id, 213, 685, 0, 0)
        sleep 4

        # 出型大片
        Tools.mouse_click(page_window_id, 274, 699, 0, 0)
        sleep 4

        if rand(2) > 0
          # 播放
          Tools.mouse_click(page_window_id, 210, 359, 0, 0)
          sleep 4
        else
          # 呼朋唤友
          Tools.mouse_click(page_window_id, 133, 688, 0, 0)
          sleep 4
        end
        sleep 8
      end
    rescue => e
      logger.warn "执行点击任务失败：#{e}".red
    ensure
      `killall chrome`
    end
    return true # device_id is correct
  end

  def self.impression_and_click(open_options, browser, click_url, async_click_url, impression_url, residence_time, logger)
    #曝光
    logger.warn "曝光"
    if impression_url and !impression_url.empty?
      open(impression_url, open_options)
      sleep rand(2..3)
    end

    #异步点击
    logger.warn "异步点击"
    if async_click_url and !async_click_url.empty?
      open(async_click_url, open_options)
    end

    #点击
    logger.warn "点击"
    if click_url and !click_url.empty?
      begin
        Timeout.timeout(rand(29..31), Errno::ETIMEDOUT) do
          browser.goto click_url
        end
      rescue => e
      ensure
      end
      sleep residence_time
    end
  end

end


